<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ContactMessageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContactMessageRepository::class)
 */
class ContactMessage
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Embedded(class=Name::class, columnPrefix = false)
     */
    private ?Name $name = null;

    /**
     * @ORM\Embedded(class=Phone::class, columnPrefix = false)
     */
    private ?Phone $phone = null;

    /**
     * @ORM\Embedded(class=Email::class, columnPrefix = false)
     */
    private ?Email $email = null;

    /**
     * @ORM\Embedded(class=Message::class, columnPrefix = false)
     */
    private ?Message $message = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?Name
    {
        return $this->name;
    }

    public function setName(?Name $name): void
    {
        $this->name = $name;
    }

    public function getPhone(): ?Phone
    {
        return $this->phone;
    }

    public function setPhone(?Phone $phone): void
    {
        $this->phone = $phone;
    }

    public function getEmail(): ?Email
    {
        return $this->email;
    }

    public function setEmail(?Email $email): void
    {
        $this->email = $email;
    }

    public function getMessage(): ?Message
    {
        return $this->message;
    }

    public function setMessage(?Message $message): void
    {
        $this->message = $message;
    }
}
