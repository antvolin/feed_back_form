<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\ContactMessage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;

class ContactMessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', NameType::class, [
                'required' => true,
                'constraints' => [new Length(['min' => 3])],
            ])
            ->add('phone', PhoneType::class, [
                'required' => true,
                'constraints' => [new Length(['min' => 13])],
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'constraints' => [new Email()],
            ])
            ->add('message', MessageType::class, [
                'required' => true,
                'constraints' => [new Length(['min' => 20])],
            ])
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ContactMessage::class,
        ]);
    }
}
